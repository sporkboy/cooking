# Shortbread

# Ingredients
* 230 grams salted butter, softened (mostly liquid)
* 115 grams granulated sugar
* 345 grams all purpose flour

# Instructions
1) Fit a square of parchment to the bottom of a 9-inch square pan. 
1) Preheat the oven to 325ºF.
1) In the bowl of an electric mixer fitted with the paddle attachment, cream the butter, sugar and salt until it is smooth, light, and creamy. Add the flour and beat it in until the mixture turns into a sandy crumble.
1) Pour the crumble mixture into the prepared pan and pat it out evenly, from edge to edge, using a flat-bottomed glass to smooth it.
1) Cut the shortbread into 16 rectangles, and dock it with a fork. Bake for 40 minutes until the edges turn golden brown.
1) Let the pan of shortbread cool for 5 minutes, then cut them again. Let them cool 10 minutes before unmolding.
1) Transfer the shortbread to a parchment-lined baking sheet, 1 inch apart, and bake for another 20 minutes or so until they are baked through.
1) Let cool before serving.