# Cinnamon Rolls

## Ingredients

### Dough
* 539g All-Purpose Flour
* 100g granulated sugar
* 227g lukewarm milk
* 74g unsalted butter, softened
* 2 large eggs, at room temperature #TODO: calculate the appropriate weight for these first run used 95g
* 14g salt
* 11g instant yeast (Or active dry yeast?)

### Filling
* 213g brown sugar
* 74g unsalted butter, softened
* 21g ground cinnamon
* A handful of chocolate chips (optional) to taste

### Icing
* 170g confectioners' sugar
* 85g cream cheese, softened
* 57g unsalted butter, softened
* 1/2 teaspoon Maple or Vanilla extract


## Instructions

### 1. Make the Dough
1) Mix together and knead all of the dough ingredients — by hand, mixer, or bread machine — to make a smooth, soft dough.
1) Place the dough in a lightly oiled bowl container. Prove until it's nearly doubled in bulk, 1 to 2 hours depending on the warmth of your kitchen.

### 2. Fill and shape the buns

1) Gently deflate the dough, and transfer it to a lightly greased work surface.
1) Roll the dough into a 16" x 21" rectangle.
1) Spread the dough with the 1/3 cup butter.
1) Mix the brown sugar and cinnamon by blitzing in a blender or food processor. This will also help break up any large brown sugar chunks.
1) Sprinkle the mixture evenly over the dough.
1) If using chololate chips spead them on top of the cinnamon and brown sugar mixture.
1) Starting with a short end, roll the dough into a log and cut it into 12 slices.
1) Place the buns in a lightly greased 9" x 13" pan. Cover the pan and prove again until they're nearly doubled, about 30 minutes.
1) near the end of the proof preheat oven to 400°F.
1) Uncover the buns, and bake until they're golden brown, about 15 minutes.
1) While the buns are baking...

### 3. Make the Icing

1) In a small bowl, beat together the cream cheese, butter, sugar, and maple extract.

### 4. Assemble

1) Remove the buns from the oven. 
1) Spread the icing on the buns while they're warm.

Serve buns warm, or at room temperature (or they will be rocks.) 
Wrap in plastic and store at room temperature for a day or so. 


## Notes
Can be frozen for longer storage.

Dough can be made ahead of time and refridgerated overnight.

