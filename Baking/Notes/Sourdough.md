# Notes on Sourdough

mostly from the tartine book



Refreshing a starter on a regular schedule with the same amount and blend of flour while storing the starter in a temperature-stable environment (ideally 65 ° to 75 ° F) will train it into a predictable and lively natural leaven. Starters that are stored at higher ambient temperatures (not refrigerated) and are more liquid favor lactic acid production over acetic acid production. The overall acid load, or concentration of acid, in the final loaf is responsible for the sour flavor. Acid load begins when the fresh leaven
