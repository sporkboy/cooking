# The Bakers Percentage Ratio 
How to calculate the correct amount of ingredients


For a given weight of flour (I.E. the flour is 100%)

For a "standard bread"
* 70% water (for a “70% hydration” dough)
* 2% salt
* 1% yeast

Need to find out for:
* eggs
* Soda (if not using yeast)
* other garbage